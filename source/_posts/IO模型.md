---
title: IO模型
tags: IO模型
comments: true
categories: IO模型
abbrlink: aeafbee0
date: 2022-03-02 21:26:00
reward:
declare:
---

首先，为了保证操作系统的安全性和稳定性，一个进程的地址空间划分为**用户空间**和**内核空间**。

用户空间的程序不能直接访问内核空间的，所以，执行 IO 操作的时候，只能发起系统调用请求操作系统帮忙完成。通过**系统调用**来间接访问内核空间。



在平时开发中，我们的程序只是对操作系统的内核发起 IO 调用（系统调用），操作系统负责的内核执行具体的 IO 操作。



当应用程序发起 I/O 调用后，会经历两个步骤：

* 内核等待 I/O 设备准备好数据。
* 内核将数据从内核空间拷贝到用户空间。



在 UNIX 系统下，IO 模型一共有5种：**同步阻塞 I/O、同步非阻塞 I/O、I/O多路复用、信号驱动 I/O  和 异步 I/O**。



#### Java 中 3 种常见 IO模型

##### BIO（Blocking I/O）

BIO 属于同步阻塞 IO 模型。



同步阻塞 IO 模型中，应用程序发起 read 调用后，会一直阻塞，直到在内核把数据拷贝到用户空间。

<img src="https://gitee.com/liangcj2020/picGo/raw/master//img/image-20220302213753291.png" alt="image-20220302213753291" style="zoom: 67%;" />

这种 IO 模型在客户端连接数量不高的情况下，是没问题的。但是，在面对十万级以上连接的时候，就无能为力。



##### NIO （Non-blocking/New I/O）

Java 中的 NIO 在 Java 1.4 中引入，对应`java.nio`包，提供了`Channel`,`Selector`,`Buffer`等抽象。它支持面向缓冲的，基于通道的 I/O 操作方法。对应高负载，高并发的应用，应该使用 NIO。



NIO 可以看做是 **I/O多路复用模型**。也有人认为它是**同步非阻塞 IO模型**。

**同步非阻塞**

<img src="https://gitee.com/liangcj2020/picGo/raw/master//img/image-20220302214527680.png" alt="image-20220302214527680" style="zoom:67%;" />

同步非阻塞 IO 模型中，应用程序会一直发起 read 调用，等待数据从内核空间拷贝到用户空间的这段时间，线程依然是阻塞的，直到内核把数据拷贝到用户空间。



相比同步阻塞 IO 模型，同步非阻塞 IO 模型确实有很大的改进。通过轮询操作，避免了一直阻塞。



但是，这种的 IO 模型同样存在问题，**应用程序不断的进行 I/O 系统调用轮询数据是否已经准备好的过程是十分消耗 CPU 资源的。**

##### I/O多路复用模型

<img src="https://gitee.com/liangcj2020/picGo/raw/master//img/image-20220302215050731.png" alt="image-20220302215050731" style="zoom:67%;" />

IO 多路复用中，线程首先发起 select 调用，询问内核数据是否已经准备就绪，等内核把数据准备好了，用户线程再发起 read 调用。read 调用过程中（数据从内核空间->用户空间) 还是阻塞的。



**IO 多路复用模型，通过减少无效的系统调用，减少了对 CPU 资源的消耗。**



NIO 中，有一个非常重要的选择器**Selector**的概念，也叫**多路复用器**。

通过他。只需要一个线程便可以管理多个客户端连接，当客户端连接到之后，才会被轮询。

<img src="https://gitee.com/liangcj2020/picGo/raw/master//img/image-20220302215552898.png" alt="image-20220302215552898" style="zoom:67%;" />

#### AIO（Asynchronous I/O）

AIO 也叫 NIO 2.是异步 IO 模型。

异步 IO 是基于事件和回调机制实现的。也就是应用操作之后会直接返回，不会阻塞在哪里，当后台处理完之后，操作系统会通知相应的线程进行后续操作。

<img src="https://gitee.com/liangcj2020/picGo/raw/master//img/image-20220302215827731.png" alt="image-20220302215827731" style="zoom:67%;" />

目前来说 AIO 的应用不是很广泛，Netty 之前也试过使用 AIO 。后面发现在 Linux 系统上的性能没有多少提升。就放弃了。

总结：

![image-20220302220021072](https://gitee.com/liangcj2020/picGo/raw/master//img/image-20220302220021072.png)
