---
title: 导入数据库文件错误
tags: 数据库
comments: true
categories: 数据库
abbrlink: 569e33a9
date: 2022-02-28 00:51:22
reward:
declare:
---

> 在导入.sql文件的时候，报错`[Err] 1231 - Variable 'sql_mode' can't be set to the value of 'NULL'`
>
> 或者系统变量`OLD_SQL_NOTES`这些不能为NULL。

<!--more-->

##### 下面是sql文件里面的源代码：

```sql
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
```

以上是在文件最后声明的，恢复我们保存的系统变量的值....

上面报错提示已经提出，所以需要在前面初始化值。

```sql

USE `sq_173cms`;

/*Table structure for table `activate_info` */
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP TABLE IF EXISTS `activate_info`;

```

**参考链接：**https://www.cnblogs.com/RGogoing/p/4497816.html
